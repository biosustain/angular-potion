var potion = angular.module('potion', []);

function copy(source, target) {
    Object.keys(source).forEach((key) => {
        target[key] = source[key];
    });
    return target;
}

function fromCamelCase(string, separator = '_') {
    return string.replace(/([a-z][A-Z])/g, (g) => `${g[0]}${separator}${g[1].toLowerCase()}`);
}

function toCamelCase(string) {
    return string.replace(/_([a-z0-9])/g, (g) => g[1].toUpperCase());
}

function omap(object, callback, thisArg) {
    var O = {};
    Object.keys(object).forEach((key) => {
        var [k, v] = callback.call(thisArg, key, object[key]);
        O[k] = v;
    });
    return O;
}


potion.provider('Api', function () {
    var provider = this;
    provider.prefix = '';

    provider.$get = ['$cacheFactory', function ($cacheFactory) {
        return {
            prefix: provider.prefix,
            defaultPerPage: 20,
            resources: {},
            cache: $cacheFactory('resource-items'),
            parseUri: function parseUri(uri) {
                uri = decodeURIComponent(uri);
                if (uri.indexOf(this.prefix) === 0) {
                    uri = uri.substring(this.prefix.length);
                }

                for (var resourceUri in this.resources) {
                    if (uri.indexOf(resourceUri) === 0) {
                        var remainder = uri.substring(resourceUri.length + 1);

                        return {
                            constructor: this.resources[resourceUri],
                            params: remainder.split('/')
                        }
                    }
                }

                throw new Error(`Unknown Resource URI: ${uri}`);
            }
        }
    }];

    return provider;
});

potion.factory('ResourceType', ['Api', 'Route', function (Api, Route) {
    class ResourceType {
        save() {
            if (this.$hasBeenSaved()) {
                return this.$route.post(this.toJSON(), null, this);
            } else {
                return this.constructor.route.post(this.toJSON(), null, this);
            }
        }

        update(changes) {
            if (this.$hasBeenSaved()) {
                return this.$route.patch(changes, null, this)
            } else {
                copy(changes, this);
                return this.save();
            }
        }

        destroy() {
            if (this.$hasBeenSaved()) {
                return this.$route
                    .destroy()
                    .then(() => {
                        Api.cache.remove(this.$uri);
                        return this;
                    });
            }

            return false;
        }

        get $id() {
            return parseInt(Api.parseUri(this.$uri).params[0]);
        }

        get $route() {
            return new Route(this.$uri);
        }

        $hasBeenSaved() {
            return !!this.$uri && this.$saved;
        }

        $ensureLoaded() {
            if (!this.$hasBeenSaved()) {
                return this.$route.get();
            }
        }

        toJSON() {
            // omit read-only fields
            // resolve promises
            var instance = {};

            Object.keys(this)
                .filter((k) => this.constructor.meta.readOnly.indexOf(k) === -1 && k != '$uri')
                .forEach((k) => {instance[fromCamelCase(k)] = this[k]});

            return instance;
        }
    }

    ResourceType.prototype.delete = ResourceType.prototype.destroy;

    return ResourceType;
}]);

potion.factory('Resource', ['$q', 'Api', 'Route', 'LazyPromise', 'ResourceType', function($q, Api, Route, LazyPromise, ResourceType) {
    var toUri = (route, id) => `${Api.prefix}${route}/${id}`;

    function cacheInstance(ctype, data) {
        var instance, uri = data.$uri;

        if (!(instance = Api.cache.get(uri))) {
            instance = new ctype(data);
            Api.cache.put(uri, instance);
        } else {
            copy(data, instance);
        }

        return instance;
    }

    function parse(data) {

    }


    return function (resourceUri, {promises=[], routes={}, instanceRoutes={}, readOnly=[]} = {}) {
        class Resource extends ResourceType {
            constructor(data) {

                constructor.meta.promises.forEach((key) => {
                    var raw = () => parse(data[key]);

                    Object.defineProperty(this, key, {
                        enumerable: false,
                        get: () => new LazyPromise(raw),
                        set: (value) => {
                            raw = value;
                        }
                    });
                });

                copy(data || {}, this);
                // TODO promises
            }
        }

        var constructor = Resource;
        var route = constructor.route = new Route(resourceUri);
        constructor.meta = {resourceUri, readOnly, promises, routes, instanceRoutes};

        constructor.empty = (id) => {
            return cacheInstance(constructor, {$uri: toUri(resourceUri, id)});
        }

        constructor.get = (id, refresh = false) => {
            var instance, uri = toUri(resourceUri, id);
            if (instance = Api.cache.get(uri)) {
                if (refresh) {
                    return instance.refresh();
                } else {
                    return $q.when(instance);  // always return a promise
                }
            }
            return new Route(uri).get();
        };

        constructor.getList = (queryParams) => {
            return route.getList(queryParams);
        };

        Object.keys(routes).forEach((key) => {
           constructor[key] = new Route(`${resourceUri}/${routes[key]}`);
        });

        Object.keys(instanceRoutes).forEach((key) => {
            Object.defineProperty(Resource.prototype, key, {
                enumerable: false,
                get: function() {
                    return new Route(`${this.$uri}${instanceRoutes[key]}`)
                }
            });
        });

        Api.resources[resourceUri] = Resource;
        return Resource;
    }
}]);

potion.factory('Route', ['$q', '$http', 'Api', 'LazyPromise', 'Pagination', function($q, $http, Api, LazyPromise, Pagination) {

    function fromJSON(instance, defaultObj=null) {
        var value, promise, promises = [];
        if(typeof instance == 'object') {
            if(instance instanceof Array) {
                var items = [];
                for (var item of instance) {
                    [value, promise] = fromJSON(item);
                    if(promise !== null) {
                        promises.push(promise)
                    }
                    items.push(value);
                }
                return [items, $q.all(promises)];
            } else if(typeof instance.$uri == 'string') {
                var uri = instance.$uri; // FIXME un-JSON data here
                var data = omap(instance, (k, v) => {
                    if (k == '$uri') {
                        return [k, v];
                    }
                    // TODO omit promise fields - move fromJSON somewhere where it can be called later
                    var [value, promise] = fromJSON(v);

                    if(promise !== null) {
                        promises.push(promise);
                    }
                    return [toCamelCase(k), value];
                });

                // TODO omit promise fields
                if(defaultObj) {
                    value = defaultObj;
                    copy(data, value);
                } else if (!(value = Api.cache.get(uri))) {
                    var {constructor} = Api.parseUri(uri);
                    value = new constructor(data);
                } else {
                    copy(data, value);
                }

                value.$uri = uri;
                value.$saved = true;
                Api.cache.put(uri, value);
                return [value, $q.all(promises)];
            } else if(typeof instance.$date != 'undefined' && Object.keys(instance).length == 1) {
                return [new Date(instance.$date), null];
            } else if(typeof instance.$ref == 'string' && Object.keys(instance).length == 1) {
                var uri = instance.$ref;
                var value = Api.cache.get(uri);

                if(typeof value == 'undefined') {
                    var {constructor, params} = Api.parseUri(uri);
                    value = constructor.empty(params[0]);
                }

                return [value, value.$ensureLoaded()];
            } else {
                var object = {};
                for(var key of Object.keys(instance)) {
                    [value, promise] = fromJSON(instance[key]);
                    object[fromCamelCase(key)] = value;
                    if(promise !== null) {
                        promises.push(promise)
                    }
                }
                return [object, $q.all(promises)];
            }
        } else {
            return [instance, null];
        }
    }

    function toJSON(instance) {
        if(typeof instance == 'object') {
            if(typeof instance.$uri == 'string') {
                // TODO if not $hasBeenSaved(), save() first.
                //if(!v.$hasBeenSaved()) {
                //    await v.save();
                //}
                return {"$ref": instance.$uri};
            } else if(instance instanceof Date) {
                return {$date: instance.getTime()}
            } else if(instance instanceof LazyPromise) {
                return instance.$raw;
            } else if(instance instanceof Array) {
                return instance.map(toJSON);
            } else {
                return omap(instance, (k, v) => [k, toJSON(v)]);
            }
        } else {
            return instance;
        }
    }

    function request(route, httpConfig, paginationObj=null, defaultObj=null) {
        return $http(httpConfig)
            .then((response) => {
                var [data, promises] = fromJSON(response.data, defaultObj);

                // XXX paginate only when explicitly requested.
                if(!paginationObj && response.headers('link')) {
                    paginationObj = new Pagination(null, null, route);
                }

                if(paginationObj) {
                    $q.when(promises).then(() => {
                        paginationObj._applyResponse(response, httpConfig, data);
                        return paginationObj;
                    });
                } else if(promises !== null) {
                    return promises.then(() => data);
                } else {
                    return data;
                }

            });
    }

    // transform

    class Route {
        constructor(uri, {prefix=Api.prefix} = {}) {
            this.uri = uri;
            this.prefix = prefix;
        }

        callWithHttpConfig(httpConfig, paginationObj = null) {
            return request(this, httpConfig, paginationObj)
        }

        get(queryParams = {}, paginate = false, cache = true) {
            var paginationObj = null;

            if(paginate) {
                var {page=1, perPage=Api.defaultPerPage} = queryParams;
                paginationObj = new Pagination(page, perPage, this);
            }

            return request(this, {
                url: `${this.prefix}${this.uri}`,
                params: omap(queryParams, (k, v) => [fromCamelCase(k), v]) || {},
                method: 'GET',
                cache: cache
            }, paginationObj);
        }

        getList(queryParams = {}, paginate = false, cache = true) {
            return this.get(queryParams, paginate, cache);
        }

        post(data, params = null, defaultObj = null) {
            return request(this, {
                url: `${this.prefix}${this.uri}`,
                data: toJSON(data || {}),
                params: params || {},
                method: 'POST',
                cache: false
            }, null, defaultObj)
        }

        patch(data, params = null, defaultObj = null) {
            return request(this, {
                url: `${this.prefix}${this.uri}`,
                data: toJSON(data || {}),
                params: params || {},
                method: 'PATCH',
                cache: false
            }, null, defaultObj)
        }

        destroy(data, params = null) {
            return request(this, {
                url: `${this.prefix}${this.uri}`,
                data: toJSON(data || {}),
                params: params || {},
                method: 'DELETE',
                cache: false
            })
        }
    }

    Route.prototype.delete = Route.prototype.destroy;
    return Route;
}]);

potion.factory('Pagination', function () {

    function parseLinkHeader(linkHeader) {
        var key, link, links, param, queryString, re, rel, url, val;
        links = {};
        re = /<([^>]+)>; rel="([a-z0-9]+),?"/g;

        if (linkHeader == null) {
            return null;
        }

        while (link = re.exec(linkHeader)) {
            [url, rel] = link.slice(1);
            links[rel] = {rel: rel, url: url};

            if (url.indexOf('?') !== -1) {
                queryString = url.substring(url.indexOf('?') + 1);
                for(param of queryString.split('&')) {
                    [key, val] = param.split(/\=/);
                    links[rel][toCamelCase(key)] = val;
                }
            }
        }
        return links;
    }

    class Pagination extends Array {

        constructor(page, perPage, route) {
            this._page = page;
            this._pages = null;
            this._perPage = perPage;
            this._route = route;
        }

        // TODO move this into Route function:
        _applyResponse(response, httpConfig, items) {
            var links = parseLinkHeader(response.headers('link'));

            if(links) {
                this._page = parseInt(links.self.page) || this._page;
                this._perPage = parseInt(links.self.perPage) || this._perPage;
                this._pages = links.last ? parseInt(links.last.page) : this._page;
            } else {
                this._page = undefined;
                this._perPage = undefined;
                this._pages = undefined;
            }

            this._httpConfig = httpConfig;

            this.length = 0;
            this.push(...items);
            return this;
        }

        map(callback, thisArg) {
            var P = new Pagination();
            P._page = this._page;
            P._pages = this._pages;
            P._perPage = this._perPage;
            P._httpConfig = this._httpConfig;
            P._route = this._route;
            P.length = this.length;

            var k = 0;
            while(k < this.length) {
                P[k] = callback.call(thisArg, this[k], k, this);
                k++;
            }

            return P;
        }

        get page() {
            return this._page;
        }

        set page(page) {
            this.changePageTo(page);
        }

        changePageTo(page) {
            var httpConfig = this._httpConfig;
            httpConfig.params.page = page;
            return this._route.callWithHttpConfig(httpConfig, this);
        }

        get pages() {
            return this._pages;
        }

        get perPage() {
            return this._perPage;
        }
    }

    return Pagination;
});

potion.factory('LazyPromise', ['$q', function($q) {
    class LazyPromise {
        constructor(raw) {
            this.$raw = raw;
        }

        get $promise() {
            if(typeof this.$raw == 'function') {
                return $q.when(this.$raw());
            }
            return $q.when(this.$raw);
        }

        then(success, error, notify) {
            return this.$promise.then(success, error, notify);
        }
    }

    LazyPromise.prototype['catch'] = function (callback) {
        return this.$promise.catch(callback);
    };

    LazyPromise.prototype['finally'] = function (callback) {
        return this.$promise.finally(callback);
    };

    return LazyPromise;
}]);