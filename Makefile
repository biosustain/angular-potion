PATH := ./node_modules/.bin:${PATH}

build:
	traceur --dir src/ dist/

dist:
	build

publish:
	dist
	npm publish