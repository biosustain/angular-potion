define(['angular', 'potion'], function () {
    describe("Angular-potion", function () {
        "use strict";
        var $httpBackend, Api, Resource, Route;

        beforeEach(module("potion"));

        beforeEach(inject(function ($injector) {

            $httpBackend = $injector.get("$httpBackend");


            var books = [
                {
                    "$uri": "/books/1",
                    "title": "Foo",
                    "publication_date": {"$date": 1422006120000}
                }
            ];

            $httpBackend.whenGET("/books").respond(books);
            $httpBackend.whenGET("/books/1").respond(books[0]);
            $httpBackend.whenGET("/books/1/new").respond(true);
            $httpBackend.whenGET("/books/2/new").respond(false);

            $httpBackend.whenGET("/foo").respond({
                "foo": "bar",
                "baz": [0.0, 1.23],
                "bat": {"$date": 1422006120000}
            });

            Api = $injector.get('Api');
            Resource = $injector.get('Resource');
            Route = $injector.get('Route');
        }));


        describe("Routes", function () {

            it("should return the JSON at a given uri on GET", function () {
                new Route('/foo').get().then(function (foo) {
                    expect(foo).toEqual({
                        "foo": "bar",
                        "baz": [0.0, 1.23],
                        "bat": new Date(1422006120000)
                    });
                });

                $httpBackend.flush();
            });
        });

        describe("Resources", function () {

            it("should read the list of items", function () {
                var Book = Resource('/books');

                Book.getList().then(function (books) {
                    expect(books).toEqual([
                        new Book({
                            "$uri": "/books/1",
                            "title": "Foo",
                            "publicationDate": new Date(1422006120000),
                            "$saved": true
                        })
                    ]);
                });

                $httpBackend.flush();
            });

            it("should access item routes", function () {
                var Book = Resource('/books', {
                    instanceRoutes: {
                        isNew: '/new'
                    }
                }, {});

                Book.get(1).then(function (book) {
                    book.isNew.get().then(function (result) {
                        expect(result).toBe(true);
                    })
                });

                Book.empty(2).isNew.get().then(function (result) {
                    expect(result).toBe(false);
                });

                $httpBackend.flush();
            });

            it("should save and update books properly", function () {
                var Book = Resource('/books', {
                    readOnly: [
                        'createdAt'
                    ]
                }, {});

                $httpBackend.expectPOST('/books', {
                    "title": "Game of Foos",
                    "publication_date": {"$date": 1286668800000}
                }).respond(201, {
                    "$uri": "/books/3",
                    "title": "Game of Foos",
                    "publication_date": {"$date": 1286668800000},
                    "created_at": {"$date": 1406892000000}
                });

                $httpBackend.expectPOST('/books/3', {
                    "$uri": "/books/3", // TODO may skip URI field (or '_' fields in general)
                    "title": "Game of Foos",
                    "publication_date": {"$date": 1286668800000},
                }).respond(200, {
                    "$uri": "/books/3",
                    "title": "Game of Foos",
                    "publication_date": {"$date": 1286668800000},
                    "created_at": {"$date": 1406892000000}
                });

                $httpBackend.expectPATCH('/books/3', {
                    "title": "Soylent Bar"
                }).respond(200, {
                    "$uri": "/books/3",
                    "title": "Soylent Bar",
                    "publication_date": {"$date": 1286668800000},
                    "created_at":  {"$date": 1406892000000}
                });

                var book = new Book({
                    title: "Game of Foos",
                    publicationDate: new Date("2010-10-10")
                });

                book.save().then(function (instance) {
                    expect(book).toBe(instance);
                    expect(instance.$uri).toBe('/books/3');
                    expect(instance.publicationDate).toEqual(new Date("2010-10-10"));
                    expect(instance.createdAt).toEqual(new Date("2014-08-01T11:20Z"));

                    book.save().then(function (instance) {
                        expect(book).toBe(instance);

                        book.update({title: "Soylent Bar"}).then(function (instance) {
                            expect(book).toBe(instance);
                            expect(book.$uri).toEqual('/books/3');
                            expect(Api.cache.get('/books/3')).toBe(book);
                            expect(instance.title).toBe("Soylent Bar");

                            $httpBackend.expectDELETE('/books/3').respond(204);

                            book.delete().then(function () {
                                expect(Api.cache.get('/books/3')).toBeUndefined();
                            });
                        });
                    });
                });

                $httpBackend.flush();
            });

//        it("should convert properties from and to camelCase", function() {
//            expect(true).toBeFalsy();
//        });

            it("should support embedded resources", function () {
                var Address = Resource('/address');
                var User = Resource('/user');

                $httpBackend.whenGET('/user/1').respond(200, {
                    "$uri": "/user/1",
                    "name": "Foo",
                    "addresses": [
                        {"$ref": "/address/1"},
                        {"$ref": "/address/2"}
                    ]
                });

                $httpBackend.expectGET('/address/1').respond(200, {
                    "$uri": "/address/1",
                    "street": "Foo St"
                });

                $httpBackend.expectGET('/address/2').respond(200, {
                    "$uri": "/address/2",
                    "street": "Bar St"
                });

                User.get(1).then(function (user) {
                    console.warn('>>>>>',user, user.addresses, Api.cache.get('/user/1'), Api.cache.get('/user/1').addresses, Api.cache.get('/address/1'))
                    expect(user.name).toBe("Foo");
                    expect(user.addresses).toEqual([
                        new Address({$uri: '/address/1', street: "Foo St", $saved: true}),
                        new Address({$uri: '/address/2', street: "Bar St", $saved: true})
                    ]);
                });

                $httpBackend.flush();
            });


            it("should support lazy embedded resources", function () {
                var Address = Resource('/address');
                var LazyUser = Resource('/user', {
                    promises: ['addresses']
                });

                $httpBackend.whenGET('/user/1').respond(200, {
                    "$uri": "/user/1",
                    "name": "Foo",
                    "addresses": [
                        {"$ref": "/address/2"}
                    ]
                });

                $httpBackend.expectGET('/address/2').respond(200, {
                    "$uri": "/address/2",
                    "street": "Bar St"
                });

                LazyUser.get(1).then(function (user) {
                    expect(user.name).toBe("Foo");
                    expect(user.addresses.then).toBeDefined();
                    console.log('---->',user, user.addresses.$raw)
                    expect(Api.cache.get('/address/1')).toBeUndefined();
                    user.addresses.then(function (addresses) {
                        expect(Api.cache.get('/address/1')).toBeDefined();
                        expect(addresses).toEqual([
                            new Address({$uri: '/address/1', street: "Foo St"}),
                            new Address({$uri: '/address/2', street: "Bar St"})
                        ]);
                    });
                });

                $httpBackend.flush();
            });


            it("should send uri references for existing objects", function () {
                var City = Resource('/city');
                var Address = Resource('/address');

                $httpBackend.expectPOST('/address', {
                    "city": {"$ref": "/city/1"},
                    "street": "Foo St"
                }).respond(200, {
                    "$uri": "/address/123",
                    "city": {"$ref": "/city/1"},
                    "street": "Bar St"
                });

                $httpBackend.expectGET('/city/1').respond(200, {
                    "$uri": "/city/1",
                    "name": "Foo"
                });

                var address = new Address({
                    city: City.empty(1),
                    street: "Foo St"
                });

                expect(Api.cache.get('/city/1')).toBeDefined();

                address.save().then(function (address) {
                    expect(address.$id).toBe(123);
                    expect(address.city.$id).toBe(1);
                });

                $httpBackend.flush();
            });

            //it("should save inline objects when they are new", function () {
            //    var City = Resource('/city');
            //    var Address = Resource('/address');
            //
            //    $httpBackend.expectPOST('/city', {
            //        "name": "Copenhagen"
            //    }).respond(200, {
            //        "$uri": "/city/1",
            //        "name": "Copenhagen"
            //    });
            //
            //    $httpBackend.expectPOST('/address', {
            //        "city": {"$ref": "/city/1"},
            //        "street": "Foo St"
            //    }).respond(200, {
            //        "$uri": "/address/123",
            //        "city": {"$ref": "/city/1"},
            //        "street": "Bar St"
            //    });
            //
            //    var address = new Address({
            //        city: new City({name: "Copenhagen"}),
            //        street: "Foo St"
            //    });
            //
            //    address.save().then(function (address) {
            //        expect(address.$id).toBe(123);
            //        expect(address.city.$id).toBe(1);
            //    });
            //
            //    $httpBackend.flush();
            //});
        });

        describe("Pagination", function () {
            it("should paginate", function () {
                var items = [
                    {"name": "foo-1"},
                    {"name": "foo-2"},
                    {"name": "foo-3"},
                    {"name": "foo-4"},
                    {"name": "foo-5"},
                    {"name": "foo-6"},
                    {"name": "foo-7"},
                    {"name": "foo-8"},
                    {"name": "foo-9"},
                    {"name": "foo-10"}
                ];

                $httpBackend.whenGET("/items").respond(items);
                $httpBackend.whenGET("/items?page=1&per_page=5").respond(
                    items.slice(0, 5), {
                        "X-Total-Count": 10,
                        "Link": '</items?page=1&per_page=5>; rel="self",' +
                        '</items?page=1&per_page=5>; rel="first",' +
                        '</items?page=2&per_page=5>; rel="last"'
                    });

                $httpBackend.whenGET("/items?page=2&per_page=5").respond(
                    items.slice(5, 10), {
                        "X-Total-Count": 10,
                        "Link": '</items?page=2&per_page=5>; rel="self",' +
                        '</items?page=1&per_page=5>; rel="first",' +
                        '</items?page=2&per_page=5>; rel="last"'
                    });

                var route = new Route('/items');

                route.get({perPage: 5}, true, true).then(function (firstPage) {
                    expect(firstPage.page).toBe(1);
                    expect(firstPage.pages).toBe(2);
                    expect(firstPage.perPage).toBe(5);
                    expect(firstPage.length).toBe(5);

                    firstPage.forEach(function (value, i) {
                        expect(value).toEqual(items[i]);
                    });

                    firstPage.changePageTo(2).then(function (secondPage) {
                        expect(firstPage).toBe(secondPage);
                        expect(firstPage.page).toBe(2);

                        secondPage.forEach(function (value, i) {
                            expect(value).toEqual(items[i + 5]);
                        });
                    })

                });

                $httpBackend.flush();

            });
        });

        afterEach(function () {
            $httpBackend.verifyNoOutstandingExpectation();
            $httpBackend.verifyNoOutstandingRequest();
        });
    });
});

// TODO convert to ES6
// test get create update delete
// test cache
// test pagination