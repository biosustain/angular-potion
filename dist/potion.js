System.register((void 0), [], function() {
  "use strict";
  var potion = angular.module('potion', []);
  function copy(source, target) {
    Object.keys(source).forEach((function(key) {
      target[key] = source[key];
    }));
    return target;
  }
  function fromCamelCase(string) {
    var separator = arguments[1] !== (void 0) ? arguments[1] : '_';
    return string.replace(/([a-z][A-Z])/g, (function(g) {
      return ("" + g[0] + separator + g[1].toLowerCase());
    }));
  }
  function toCamelCase(string) {
    return string.replace(/_([a-z0-9])/g, (function(g) {
      return g[1].toUpperCase();
    }));
  }
  function omap(object, callback, thisArg) {
    var O = {};
    Object.keys(object).forEach((function(key) {
      var $__6 = $traceurRuntime.assertObject(callback.call(thisArg, k, v[k])),
          k = $__6[0],
          v = $__6[1];
      O[k] = v;
    }));
    return O;
  }
  potion.provider('Api', function() {
    var provider = this;
    provider.prefix = '';
    provider.$get = ['$cacheFactory', function($cacheFactory) {
      return {
        prefix: provider.prefix,
        defaultPerPage: 20,
        resources: {},
        cache: $cacheFactory('resource-items'),
        parseUri: function parseUri(uri) {
          uri = decodeURIComponent(uri);
          if (uri.indexOf(this.prefix) === 0) {
            uri = uri.substring(this.prefix.length);
          }
          for (var resourceUri in this.resources) {
            if (uri.indexOf(resourceUri) === 0) {
              var remainder = uri.substring(resourceUri.length + 1);
              return {
                constructor: this.resources[resourceUri],
                params: remainder.split('/')
              };
            }
          }
          throw new Error(("Unknown Resource URI: " + uri));
        },
        fromUri: function(uri) {
          var instance = this.cache.get(uri);
          if (instance) {
            return instance;
          } else {
            var $__6 = $traceurRuntime.assertObject(this.parseUri(uri)),
                constructor = $__6.constructor,
                params = $__6.params;
            return constructor.empty(uri);
          }
        }
      };
    }];
    return provider;
  });
  potion.factory('ResourceType', ['Api', function(Api) {
    var ResourceType = function ResourceType() {};
    ($traceurRuntime.createClass)(ResourceType, {
      get: function(id) {},
      getList: function() {},
      save: function() {
        var $__0 = this;
        if (this.$hasBeenSaved()) {
          return this.$route.post(this.toJSON()).then((function(data) {
            return copy(data, $__0);
          }));
        } else {
          return this.constructor.route.post(this.toJSON()).then((function(data) {
            var uri = data.$uri,
                instance = Api.cache.get(uri);
            if (!instance) {
              instance = $__0;
              Api.cache.put(uri, instance);
            }
            copy(data, instance);
            return $__0;
          }));
        }
      },
      update: function(changes) {
        var $__0 = this;
        if (this.$hasBeenSaved()) {
          return this.$route.patch(changes).then((function(data) {
            copy(data, $__0);
            return $__0;
          }));
        } else {
          copy(changes, this);
          return this.save();
        }
      },
      destroy: function() {
        var $__0 = this;
        if (this.$hasBeenSaved()) {
          return this.$route.destroy().then((function() {
            Api.cache.remove($__0.$uri);
            return $__0;
          }));
        }
        return false;
      },
      get $id() {
        return parseInt(Api.parseUri(this.$uri).params[0]);
      },
      get $route() {
        return new Route(this.$uri);
      },
      $hasBeenSaved: function() {
        return !!this.$uri;
      },
      $ensureLoaded: function() {
        if (this.$hasBeenSaved()) {
          return this.$route.get();
        }
      },
      toJSON: function() {}
    }, {});
    return ResourceType;
  }]);
  potion.factory('Resource', ['Api', 'Route', 'LazyPromise', 'ResourceType', function(Api, Route, LazyPromise, ResourceType) {
    var toUri = (function(route, id) {
      return ("" + Api.prefix + route + "/" + id);
    });
    function cacheInstance(ctype, data) {
      var instance,
          uri = data.$uri;
      if (!(instance = Api.cache.get(uri))) {
        instance = new ctype(data);
        Api.cache.put(uri, instance);
      } else {
        copy(data, instance);
      }
      return instance;
    }
    function parse(data) {}
    return function(resourceUri) {
      var $__7,
          $__8,
          $__9;
      var $__6 = $traceurRuntime.assertObject(arguments[1] !== (void 0) ? arguments[1] : {}),
          promises = ($__7 = $__6.promises) === void 0 ? [] : $__7,
          routes = ($__8 = $__6.routes) === void 0 ? {} : $__8,
          instanceRoutes = ($__9 = $__6.instanceRoutes) === void 0 ? {} : $__9,
          $__11 = $traceurRuntime.assertObject($__6.readOnly);
      var Resource = function Resource(data) {
        var $__0 = this;
        constructor.meta.promises.forEach((function(key) {
          var raw = (function() {
            return parse(data[key]);
          });
          Object.defineProperty($__0, key, {
            enumerable: false,
            get: (function() {
              return LazyPromise(raw);
            }),
            set: (function(value) {
              raw = value;
            })
          });
        }));
        copy(data || {}, this);
        constructor.meta.instanceRoutes.forEach((function(route) {
          Object.defineProperty($__0, route, {
            enumerable: false,
            get: (function() {
              return new Route(($__0.$uri + "/" + fromCamelCase(route, '-')));
            })
          });
        }));
      };
      ($traceurRuntime.createClass)(Resource, {}, {}, ResourceType);
      var constructor = Resource;
      constructor.meta = {
        resourceUri: resourceUri,
        promises: promises,
        routes: routes,
        instanceRoutes: instanceRoutes
      };
      constructor.route = new Route(resourceUri);
      constructor.empty = (function(id) {
        return cacheInstance(constructor, {$uri: toUri(resourceUri, id)});
      });
      routes.forEach((function(route) {
        constructor[route] = new Route((resourceUri + "/" + fromCamelCase(route, '-')));
      }));
      return constructor;
    };
  }]);
  potion.factory('Route', ['$q', '$http', 'Api', 'LazyPromise', 'ResourceType', function($q, $http, Api, LazyPromise, ResourceType) {
    function fromJSON(instance) {
      var $__7,
          $__8;
      var value,
          promise,
          promises = [];
      if (typeof instance == 'object') {
        if (instance instanceof Array) {
          var items = [];
          for (var $__2 = instance[Symbol.iterator](),
              $__3; !($__3 = $__2.next()).done; ) {
            var item = $__3.value;
            {
              ($__7 = $traceurRuntime.assertObject(fromJSON(item)), value = $__7[0], promise = $__7[1], $__7);
              if (promise !== null) {
                promises.push(promises);
              }
              items.push(value);
            }
          }
          return [items, $q.all(promises)];
        } else if (typeof instance.$uri == 'string') {
          var uri = instance.$uri,
              data = fromJSON(instance);
          if (!(value = Api.cache.get(uri))) {
            var constructor = $traceurRuntime.assertObject(Api.parseUri(uri)).constructor;
            value = new constructor(data);
            Api.cache.put(uri, instance);
          } else {
            copy(data, value);
          }
          return [value, null];
        } else if (typeof instance.$date != 'undefined' && Object.keys(instance).length == 1) {
          return [new Date(instance.$date), null];
        } else if (typeof instance.$ref != 'undefined' && Object.keys(instance).length == 1) {
          instance = Api.fromUri(instance.$ref);
          return [instance, instance.$ensureLoaded()];
        } else {
          var object = {};
          for (var $__4 = Object.keys(instance)[Symbol.iterator](),
              $__5; !($__5 = $__4.next()).done; ) {
            var key = $__5.value;
            {
              ($__8 = $traceurRuntime.assertObject(fromJSON(instance[key])), value = $__8[0], promise = $__8[1], $__8);
              object[fromCamelCase(key)] = value;
              if (promise !== null) {
                promises.push(promises);
              }
            }
          }
          return [object, $q.all(promises)];
        }
      } else {
        return [instance, null];
      }
    }
    function toJSON(instance) {
      if (typeof instance == 'object') {
        if (v instanceof ResourceType) {
          return {$ref: v.$uri};
        } else if (v instanceof Date) {
          return {$date: v.getTime()};
        } else if (v instanceof LazyPromise) {
          return v.$raw;
        }
      } else {
        return instance;
      }
    }
    function request(route, httpConfig) {
      var paginationObj = arguments[2] !== (void 0) ? arguments[2] : null;
      return $http(httpConfig).then((function(response) {
        var $__7 = $traceurRuntime.assertObject(fromJSON(response.data)),
            data = $__7[0],
            promises = $__7[1];
        if (!paginationObj && response.headers('link')) {
          paginationObj = new Pagination(null, null, route);
        }
        if (paginationObj) {
          $q.when(promises).then((function() {
            paginationObj._applyResponse(response, httpConfig, data);
            return paginationObj;
          }));
        } else if (promises) {
          return promises.then((function() {
            return data;
          }));
        } else {
          return data;
        }
      }));
    }
    var Route = function Route(uri) {
      var $__8;
      var $__7 = $traceurRuntime.assertObject(arguments[1] !== (void 0) ? arguments[1] : {}),
          prefix = ($__8 = $__7.prefix) === void 0 ? Api.prefix : $__8;
      this.uri = uri;
      this.prefix = prefix;
    };
    ($traceurRuntime.createClass)(Route, {
      callWithHttpConfig: function(httpConfig) {
        var paginationObj = arguments[1] !== (void 0) ? arguments[1] : null;
        return request(this, httpConfig, paginationObj);
      },
      get: function() {
        var $__7,
            $__9;
        var queryParams = arguments[0] !== (void 0) ? arguments[0] : {};
        var paginate = arguments[1] !== (void 0) ? arguments[1] : false;
        var cache = arguments[2] !== (void 0) ? arguments[2] : true;
        var paginationObj = null;
        if (paginate) {
          var $__8 = $traceurRuntime.assertObject(queryParams),
              page = ($__7 = $__8.page) === void 0 ? 1 : $__7,
              perPage = ($__9 = $__8.perPage) === void 0 ? Api.defaultPerPage : $__9;
          paginationObj = new Pagination(page, perPage, this);
        }
        return request(this, {
          url: ("" + this.prefix + this.uri),
          params: omap(queryParams, (function(k, v) {
            return [fromCamelCase(k), v];
          })) || {},
          method: 'GET',
          cache: cache
        }, paginationObj);
      },
      getList: function() {
        var queryParams = arguments[0] !== (void 0) ? arguments[0] : {};
        var paginate = arguments[1] !== (void 0) ? arguments[1] : true;
        var cache = arguments[2] !== (void 0) ? arguments[2] : true;
        return this.get(queryParams, paginate, cache);
      },
      post: function(data) {
        var params = arguments[1] !== (void 0) ? arguments[1] : null;
        return request(this, {
          url: ("" + this.prefix + this.uri),
          data: toJSON(data || {}),
          params: params || {},
          method: 'POST',
          cache: false
        });
      },
      patch: function(data) {
        var params = arguments[1] !== (void 0) ? arguments[1] : null;
        return request(this, {
          url: ("" + this.prefix + this.uri),
          data: toJSON(data || {}),
          params: params || {},
          method: 'PATCH',
          cache: false
        });
      },
      destroy: function(data) {
        var params = arguments[1] !== (void 0) ? arguments[1] : null;
        return request(this, {
          url: ("" + this.prefix + this.uri),
          data: toJSON(data || {}),
          params: params || {},
          method: 'DELETE',
          cache: false
        });
      }
    }, {});
    Route.prototype.delete = Route.prototype.destroy;
    return Route;
  }]);
  potion.factory('Pagination', function() {
    function parseLinkHeader(linkHeader) {
      var $__7,
          $__9;
      var key,
          link,
          links,
          param,
          queryString,
          re,
          rel,
          url,
          val;
      links = {};
      re = /<([^>]+)>; rel="([a-z0-9]+),?"/g;
      if (linkHeader == null) {
        return null;
      }
      while (link = re.exec(linkHeader)) {
        ($__7 = $traceurRuntime.assertObject(link.slice(1)), url = $__7[0], rel = $__7[1], $__7);
        links[rel] = {
          rel: rel,
          url: url
        };
        if (url.indexOf('?') !== -1) {
          queryString = url.substring(url.indexOf('?') + 1);
          for (var $__2 = queryString.split('&')[Symbol.iterator](),
              $__3; !($__3 = $__2.next()).done; ) {
            param = $__3.value;
            {
              ($__9 = $traceurRuntime.assertObject(param.split(/\=/)), key = $__9[0], val = $__9[1], $__9);
              links[rel][toCamelCase(key)] = val;
            }
          }
        }
      }
      return links;
    }
    var Pagination = function Pagination(page, perPage, route) {
      this._page = page;
      this._pages = null;
      this._perPage = perPage;
      this._route = route;
    };
    var $Pagination = Pagination;
    ($traceurRuntime.createClass)(Pagination, {
      _applyResponse: function(response, httpConfig, items) {
        var $__12;
        var links = parseLinkHeader(response.headers('link'));
        if (links) {
          this._page = parseInt(links.self.page) || this._page;
          this._perPage = parseInt(links.self.perPage) || this._perPage;
          this._pages = links.last ? parseInt(links.last.page) : this._page;
        } else {
          this._page = undefined;
          this._perPage = undefined;
          this._pages = undefined;
        }
        this._httpConfig = httpConfig;
        this.length = 0;
        ($__12 = this).push.apply($__12, $traceurRuntime.spread(items));
        return this;
      },
      map: function(callback, thisArg) {
        var P = new $Pagination();
        P._page = this._page;
        P._pages = this._pages;
        P._perPage = this._perPage;
        P._httpConfig = this._httpConfig;
        P._route = this._route;
        P.length = this.length;
        var k = 0;
        while (k < this.length) {
          P[k] = callback.call(thisArg, this[k], k, this);
          k++;
        }
        return P;
      },
      get page() {
        return this._page;
      },
      set page(page) {
        this.changePageTo(page);
      },
      changePageTo: function(page) {
        var httpConfig = this._httpConfig;
        httpConfig.params.page = page;
        return this._route.callWithHttpConfig(httpConfig, this);
      },
      get pages() {
        return this._pages;
      },
      get perPage() {
        return this._perPage;
      }
    }, {}, Array);
    return Pagination;
  });
  potion.factory('LazyPromise', ['$q', function($q) {
    var LazyPromise = function LazyPromise(raw) {
      this.$raw = raw;
    };
    ($traceurRuntime.createClass)(LazyPromise, {
      get $promise() {
        if (typeof this.$raw == 'function') {
          return $q.when(this.$raw());
        }
        return $q.when(this.$raw);
      },
      then: function(success, error, notify) {
        return this.$promise.then(success, error, notify);
      }
    }, {});
    LazyPromise.prototype['catch'] = function(callback) {
      return this.$promise.catch(callback);
    };
    LazyPromise.prototype['finally'] = function(callback) {
      return this.$promise.finally(callback);
    };
    return LazyPromise;
  }]);
  return {};
});
